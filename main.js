const fs = require('fs')
let hotel = undefined;
const Hotel = require('./hotel').Hotel;
const Guest = require("./guest").Guest;

class Command {
    constructor(commandName, params) {
        this.name = commandName;
        this.params = params;
    }
}

function main() {
    const fileName = 'input.txt'
    const commands = getCommandsFromFileName(fileName);

    commands.forEach(command => {
        switch (command.name) {
            case "create_hotel": {
                const [numberOfFloors, roomsPerFloor] = command.params;

                hotel = new Hotel(numberOfFloors, roomsPerFloor);

                console.log(`Hotel created with ${numberOfFloors} floor(s), ${roomsPerFloor} room(s) per floor.`);
                break;
            }
            case "book": {
                const [roomNumber, guestName, guestAge] = command.params;
                const guest = new Guest(guestName, guestAge);

                hotel.checkIn(roomNumber.toString(), guest);
                break;
            }
            case "list_available_rooms": {
                const availableRooms = hotel.getAvailableRooms();
                console.log(availableRooms.map(room => room.number).join(' '));
                break;
            }
            case "checkout": {
                const [keycardNumber, guestName] = command.params;

                hotel.checkOut(guestName, keycardNumber);
                break;
            }
            case "list_guest": {
                const guests = hotel.getGuests();

                const guestNames = guests.map(guest => guest.name);

                console.log(guestNames.join(', '));
                break;
            }
            case "get_guest_in_room": {
                const [roomNumber] = command.params;

                const guest = hotel.getGuestFromRoomNumber(roomNumber.toString());

                console.log(guest.name);
                break;
            }
            case "list_guest_by_age": {
                const [sign, age] = command.params;

                const guests = hotel.getGuestsByAge(sign, age);

                console.log(guests.map(guest => guest.name).join(', '));
                break;
            }
            case "list_guest_by_floor": {
                const [floorNumber] = command.params;
                const guests = hotel.getGuestsByFloor(floorNumber.toString());

                console.log(guests.map(guest => guest.name).join(', '));
                break;
            }
            case "checkout_guest_by_floor": {
                const [floor] = command.params;

                hotel.checkOutByFloor(floor.toString());
                break;
            }
            case "book_by_floor": {
                const [floorNumber, guestName, guestAge] = command.params;
                const guest = new Guest(guestName, guestAge);

                hotel.checkInByFloor(floorNumber.toString(), guest);
                break;
            }
            default:
                return;
        }
    })
}

function getCommandsFromFileName(filename) {
    const file = fs.readFileSync(filename, 'utf-8');
    return file.split('\n')
        .map(line => line.split(' '))
        .map(([commandName, ...params]) => new Command(commandName,
            params.map(param => {
                const parsedParam = parseInt(param, 10);
                return Number.isNaN(parsedParam) ? param : parsedParam;
            })
        ));
}

main();