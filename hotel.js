const Room = require("./room").Room;
const Keycard = require("./keycard").Keycard;
const BookingDetail = require("./bookingDetail").BookingDetail;

function flatten(arr) {
    return arr.reduce(function (flat, toFlatten) {
        return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []);
}

class Hotel {
    constructor(numberOfFloors, roomsPerFloor) {
        const totalNumberOfRooms = numberOfFloors * roomsPerFloor;
        this.currentBookingDetailList = [];

        this.roomList = this.createRoomList(numberOfFloors, roomsPerFloor);
        this.keycardList = this.createKeycardList(totalNumberOfRooms);
    }

    createRoomList(numberOfFloors, roomsPerFloor) {
        const floors = Array.from(new Array(numberOfFloors));
        const rooms = Array.from(new Array(roomsPerFloor));

        return floors.map(() =>
            rooms
        ).map((floor, floorCount) => {
            return floor.map((_, roomCount) => {
                const roomNumber = (floorCount + 1).toString() + (roomCount + 1).toString().padStart(2, '0');

                return new Room(roomNumber);
            })
        });
    }

    createKeycardList(totalNumberOfRooms) {
        const keycardList = Array.from(new Array(totalNumberOfRooms));

        return keycardList.map((_, keycardCount) => new Keycard(keycardCount + 1));
    }

    checkIn(roomNumber, guest) {
        const room = flatten(this.roomList).find(room => room.number === roomNumber);

        if (!room.vacant) {
            console.log(`Cannot book room ${roomNumber} for ${guest.name}, The room is currently booked by ${this.getGuestFromRoomNumber(roomNumber).name}.`);

            return;
        }

        room.book();
        this.recordBookingDetail(room, guest);
        const keycard = this.getKeycardForRoom(room);

        console.log(`Room ${room.number} is booked by ${guest.name} with keycard number ${keycard.number}.`);
    }

    checkOut(guestName, keycardNumber) {
        const guest = this.getGuestFromKeycardNumber(keycardNumber);

        if (guest.name != guestName) {
            console.log(`Only ${guest.name} can checkout with keycard number ${keycardNumber}.`)

            return;
        }

        const room = this.getRoomFromKeycardNumber(keycardNumber);
        const keycard = this.getKeycardFromKeycardNumber(keycardNumber);

        room.setVacant();
        keycard.clearRoomNumber();
        this.removeBookingDetail(room);

        console.log(`Room ${room.number} is checkout.`);
    }

    getAvailableRooms() {
        return flatten(this.roomList).filter(room => room.vacant);
    }

    getGuests() {
        const guests = new Set();

        this.currentBookingDetailList.forEach(bookingDetail => {
            guests.add(bookingDetail.guest);
        });

        return Array.from(guests);
    }

    getGuestFromRoomNumber(roomNumber) {
        const bookingDetail = this.currentBookingDetailList.find(bookingDetail => bookingDetail.room.number === roomNumber);

        return bookingDetail.guest;
    }

    checkOutByFloor(floorNumber) {
        const roomsBookedOnFloor = this.getRoomsBookedOnFloor(floorNumber);
        const keycardsOnFloor = roomsBookedOnFloor.map(room => room.number)
            .map(roomNumber => this.getKeycardFromRoomNumber(roomNumber));

        roomsBookedOnFloor.forEach(room => {
            this.removeBookingDetail(room);
            room.setVacant();
        });
        keycardsOnFloor.forEach(keycard => keycard.clearRoomNumber());

        const roomNumbersBookedOnFloor = roomsBookedOnFloor.map(room => room.number);

        console.log(`Room ${roomNumbersBookedOnFloor.join(', ')} are checkout`);
    }

    getGuestsByAge(operator, age) {
        const guests = this.getGuests();

        switch (operator) {
            case ('<'): return guests.filter(guest => guest.age < age);
            case ('>'): return guests.filter(guest => guest.age > age);
            case ('='): return guests.filter(guest => guest.age === age);
        }
    }

    recordBookingDetail(room, guest) {
        this.currentBookingDetailList.push(new BookingDetail(room, guest));
    }

    getKeycardForRoom(room) {
        const keycard = this.keycardList.find(keycard => !keycard.roomNumber);
        keycard.assignToRoomNumber(room.number);

        return keycard;
    }

    getGuestsByFloor(floorNumber) {
        const guests = this.currentBookingDetailList.filter(bookingDetail => bookingDetail.room.number.substr(0, 1) === floorNumber)
            .map(bookingDetail => bookingDetail.guest);

        return Array.from(new Set(guests));
    }

    getRoomsBookedOnFloor(floorNumber) {
        return this.currentBookingDetailList.filter(bookingDetail => bookingDetail.room.number.substr(0, 1) === floorNumber)
            .map(bookingDetail => bookingDetail.room);
    }

    getRoomsOnFloor(floorNumber) {
        return flatten(this.roomList).filter(room => room.number.substr(0, 1) === floorNumber);
    }

    removeBookingDetail(room) {
        this.currentBookingDetailList = this.currentBookingDetailList.filter(bookingDetail => bookingDetail.room !== room);
    }

    getRoomNumberFromKeycardNumber(keycardNumber) {
        return this.keycardList.find(keycard => keycard.number === keycardNumber).roomNumber;
    }

    getKeycardFromKeycardNumber(keycardNumber) {
        return this.keycardList.find(keycard => keycard.number === keycardNumber);
    }

    getKeycardFromRoomNumber(roomNumber) {
        return this.keycardList.find(keycard => keycard.roomNumber === roomNumber);
    }

    getRoomFromKeycardNumber(keycardNumber) {
        const roomNumber = this.getRoomNumberFromKeycardNumber(keycardNumber);

        return this.currentBookingDetailList.find(bookingDetail => bookingDetail.room.number == roomNumber).room;
    }

    getGuestFromKeycardNumber(keycardNumber) {
        const roomNumber = this.getRoomNumberFromKeycardNumber(keycardNumber);

        return this.getGuestFromRoomNumber(roomNumber);
    }

    isNotVacantFloor(floorNumber) {
        return this.currentBookingDetailList.find(bookingDetail => bookingDetail.room.number.substr(0, 1) === floorNumber)
    }

    checkInByFloor(floorNumber, guest) {
        if (this.isNotVacantFloor(floorNumber)) {
            console.log(`Cannot book floor ${floorNumber} for ${guest.name}`);

            return;
        }

        const roomsOnFloor = this.getRoomsOnFloor(floorNumber);
        const keycardNumbers = [];

        roomsOnFloor.forEach(room => {
            room.book();
            this.recordBookingDetail(room, guest);
            keycardNumbers.push(this.getKeycardForRoom(room).number);
        });

        const roomNumbers = roomsOnFloor.map(room => room.number);

        console.log(`Room ${roomNumbers.join(', ')} are booked with keycard number ${keycardNumbers.join(', ')}`);
    }
}

exports.Hotel = Hotel;