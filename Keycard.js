class Keycard {
    constructor(keycardNumber) {
        this.number = keycardNumber;
        this.roomNumber = undefined;
    }

    assignToRoomNumber(roomNumber) {
        this.roomNumber = roomNumber;
    }

    clearRoomNumber() {
        this.roomNumber = undefined;
    }
}

exports.Keycard = Keycard