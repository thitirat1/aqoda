class BookingDetail {
    constructor(room, guest) {
        this.room = room;
        this.guest = guest;
    }
}

exports.BookingDetail = BookingDetail