class Guest {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}

exports.Guest = Guest;