class Room {
    constructor(roomNumber) {
        this.number = roomNumber;
        this.vacant = true;
    }

    book() {
        this.vacant = false;
    }

    setVacant() {
        this.vacant = true;
    }
}

exports.Room = Room